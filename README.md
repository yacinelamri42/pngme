# Pngme
### Project Description
This project is used to hide messages in png files and decode messages from png files

### Installation
- Step 1: Install rust using https://www.rust-lang.org/tools/install
- Step 2: Run this command
```bash
cargo install --git https://gitlab.com/yacinelamri42/pngme.git
```
### Usage  
