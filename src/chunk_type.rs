use std::{str::FromStr, fmt::Display, write};
use std::error::Error;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ChunkType {
    chunks: [u8;4]
}
#[derive(Debug)]
pub enum ChunkTypeErrors{
    InvalidInput,
    ChunkNotValid
}

impl Display for ChunkTypeErrors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidInput => write!(f, "The input is invalid"),
            Self::ChunkNotValid => write!(f, "The chunk is invalid")
        }
    }
}

impl Error for ChunkTypeErrors {
    
}


impl ChunkType {
    pub fn bytes(&self) -> [u8;4] {
        self.chunks
    }

    fn is_valid(&self) -> bool {
        for num in self.chunks {
            if !((num >= 65 && num <= 90) || (num >= 97 && num <= 122)) {
                return false;
            }
        }
        true && self.is_reserved_bit_valid()
    }

    fn is_critical(&self) -> bool {
        (self.chunks[0] & 1 << 5) == 0
    }

    fn is_public(&self) -> bool {
        (self.chunks[1] & 1 << 5) == 0

    }

    fn is_reserved_bit_valid(&self) -> bool {
        (self.chunks[2] & 1 << 5) == 0
    }
    
    fn is_safe_to_copy(&self) -> bool {
        (self.chunks[3] & 1 << 5) != 0
    }
}

impl TryFrom<[u8;4]> for ChunkType {
    type Error = ChunkTypeErrors;
    fn try_from(value: [u8;4]) -> Result<Self, Self::Error>{
        let chunk = Self { chunks: value };
        if !chunk.is_valid(){
            return Err(ChunkTypeErrors::ChunkNotValid);
        }
        Ok(chunk)
    }
}

impl FromStr for ChunkType {
    type Err = ChunkTypeErrors;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() != 4 {
            return Err(ChunkTypeErrors::InvalidInput);
        }

        let mut arr: [u8;4] = [0,0,0,0]; 
        for (index,ch) in s.chars().enumerate() {
            if ch.is_numeric() {
                return Err(ChunkTypeErrors::InvalidInput);
            }
            arr[index] = ch as u8;
        }
        let chunk = Self { chunks: arr };
        Ok(chunk)
    }
}

impl Display for ChunkType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut to_string: String = String::new();
        self.chunks.iter().map(|num| *num as char).for_each(|ch| to_string.push(ch));

        write!(f, "{}", to_string)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryFrom;
    use std::str::FromStr;

    #[test]
    pub fn test_chunk_type_from_bytes() {
        let expected = [82, 117, 83, 116];
        let actual = ChunkType::try_from([82, 117, 83, 116]).unwrap();

        assert_eq!(expected, actual.bytes());
    }

    #[test]
    pub fn test_chunk_type_from_str() {
        let expected = ChunkType::try_from([82, 117, 83, 116]).unwrap();
        let actual = ChunkType::from_str("RuSt").unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    pub fn test_chunk_type_is_critical() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(chunk.is_critical());
    }

    #[test]
    pub fn test_chunk_type_is_not_critical() {
        let chunk = ChunkType::from_str("ruSt").unwrap();
        assert!(!chunk.is_critical());
    }

    #[test]
    pub fn test_chunk_type_is_public() {
        let chunk = ChunkType::from_str("RUSt").unwrap();
        assert!(chunk.is_public());
    }

    #[test]
    pub fn test_chunk_type_is_not_public() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(!chunk.is_public());
    }

    #[test]
    pub fn test_chunk_type_is_reserved_bit_valid() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(chunk.is_reserved_bit_valid());
    }

    #[test]
    pub fn test_chunk_type_is_reserved_bit_invalid() {
        let chunk = ChunkType::from_str("Rust").unwrap();
        assert!(!chunk.is_reserved_bit_valid());
    }

    #[test]
    pub fn test_chunk_type_is_safe_to_copy() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(chunk.is_safe_to_copy());
    }

    #[test]
    pub fn test_chunk_type_is_unsafe_to_copy() {
        let chunk = ChunkType::from_str("RuST").unwrap();
        assert!(!chunk.is_safe_to_copy());
    }

    #[test]
    pub fn test_valid_chunk_is_valid() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert!(chunk.is_valid());
    }

    #[test]
    pub fn test_invalid_chunk_is_valid() {
        let chunk = ChunkType::from_str("Rust").unwrap();
        assert!(!chunk.is_valid());

        let chunk = ChunkType::from_str("Ru1t");
        assert!(chunk.is_err());
    }

    #[test]
    pub fn test_chunk_type_string() {
        let chunk = ChunkType::from_str("RuSt").unwrap();
        assert_eq!(&chunk.to_string(), "RuSt");
    }

    #[test]
    pub fn test_chunk_type_trait_impls() {
        let chunk_type_1: ChunkType = TryFrom::try_from([82, 117, 83, 116]).unwrap();
        let chunk_type_2: ChunkType = FromStr::from_str("RuSt").unwrap();
        let _chunk_string = format!("{}", chunk_type_1);
        let _are_chunks_equal = chunk_type_1 == chunk_type_2;
    }
}
