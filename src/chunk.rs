use std::error::Error;
use std::fmt::Display;

use crate::chunk_type;

#[derive(Debug)]
pub struct Chunk {
    type_data: crate::chunk_type::ChunkType,
    data: Vec<u8>
}
#[derive(Debug)]
pub enum ChunkErrors {
    ChunkTooSmall(usize),
    DataCorrupted,
}

impl Error for ChunkErrors {}

impl Display for ChunkErrors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ChunkErrors::ChunkTooSmall(size) => write!(f,"The Chunk size is too small({size})"),
            ChunkErrors::DataCorrupted => write!(f,"The Chunk data is corrupted")
        }
    }
}

impl TryFrom<&[u8]> for Chunk {
    type Error = ChunkErrors;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        if value.len() < 12 {
            return Err(ChunkErrors::ChunkTooSmall(value.len()));
        }
        let len: &[u8; 4] = &value[0..4].try_into().unwrap();
        let type_data_slice: [u8; 4] = value[4..8].try_into().unwrap();
        let type_data = chunk_type::ChunkType::try_from(type_data_slice).unwrap();
        let length: usize = u32::from_be_bytes(*len).try_into().unwrap();
        let data = &value[8..(8+length)];
        let data: Vec<u8> = data.to_owned();
        let chunk = Chunk {type_data, data};
        let crc_shown = &value[(8+length)..(12+length)];
        if u32::from_be_bytes(crc_shown.try_into().unwrap()) != chunk.crc() {
            return Err(ChunkErrors::DataCorrupted);
        }
        Ok(chunk)
    }
}

impl Display for Chunk {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.data_as_string().unwrap())
    }
}

impl Chunk {
    pub fn new(type_data: chunk_type::ChunkType, data: Vec<u8>) -> Self {
        Self { type_data, data }
    }

    fn length(&self) -> u32 {
        self.data.len() as u32
    }

    pub fn chunk_type(&self) -> &chunk_type::ChunkType {
        &self.type_data
    }

    pub fn data(&self) -> &[u8] {
        &self.data[..]
    }

    fn crc(&self) -> u32 {
        let mut crc_table: [u32;256] = [0;256];
        let mut c: u32;
        for n in 0..256 {
            c = n as u32;
            for _k in 0..8 {
                if c & 1 == 1 {
                    c = 0xedb88320u32 ^ (c >> 1);
                }else {
                    c = c >> 1;
                }
            }
            crc_table[n] = c;
        }
        let mut buffer: Vec<u8> = Vec::new();
        let data = self.data();
        let type_data = self.type_data.bytes();
        buffer.extend_from_slice(&type_data);
        buffer.extend_from_slice(data);
        let mut crc: u32 = !0u32;
        for n in 0..(buffer.len()) {
            let index = ((crc ^ (buffer[n] as u32)) & (!0u8 as u32)) as usize;
            crc = crc_table[index] ^ (crc >> 8);
        }
        crc ^ !0u32
    }

    pub fn data_as_string(&self) -> Result<String, ChunkErrors> {
        let mut result: String = String::new();
        self.data.iter().for_each(|num| result.push(*num as char));
        Ok(result)
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        let mut result: Vec<u8> = Vec::new();
        let length = self.length().to_be_bytes();
        let type_data = self.type_data.bytes();
        let crc = self.crc().to_be_bytes();
        let data = self.data();
        result.extend_from_slice(&length);
        result.extend_from_slice(&type_data);
        result.extend_from_slice(data);
        result.extend_from_slice(&crc);
        result
    }

}



#[allow(unused_variables)]
#[cfg(test)]
mod tests {
    use super::*;
    use crate::chunk_type::ChunkType;
    use std::str::FromStr;

    fn testing_chunk() -> Chunk {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();
        
        Chunk::try_from(chunk_data.as_ref()).unwrap()
    }

    #[test]
    fn test_new_chunk() {
        let chunk_type = ChunkType::from_str("RuSt").unwrap();
        let data = "This is where your secret message will be!".as_bytes().to_vec();
        let chunk = Chunk::new(chunk_type, data);
        assert_eq!(chunk.length(), 42);
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_chunk_length() {
        let chunk = testing_chunk();
        assert_eq!(chunk.length(), 42);
    }

    #[test]
    fn test_chunk_type() {
        let chunk = testing_chunk();
        assert_eq!(chunk.chunk_type().to_string(), String::from("RuSt"));
    }

    #[test]
    fn test_chunk_string() {
        let chunk = testing_chunk();
        let chunk_string = chunk.data_as_string().unwrap();
        let expected_chunk_string = String::from("This is where your secret message will be!");
        assert_eq!(chunk_string, expected_chunk_string);
    }

    #[test]
    fn test_chunk_crc() {
        let chunk = testing_chunk();
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_valid_chunk_from_bytes() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk = Chunk::try_from(chunk_data.as_ref()).unwrap();

        let chunk_string = chunk.data_as_string().unwrap();
        let expected_chunk_string = String::from("This is where your secret message will be!");

        assert_eq!(chunk.length(), 42);
        assert_eq!(chunk.chunk_type().to_string(), String::from("RuSt"));
        assert_eq!(chunk_string, expected_chunk_string);
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_invalid_chunk_from_bytes() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656333;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk = Chunk::try_from(chunk_data.as_ref());

        assert!(chunk.is_err());
    }

    #[test]
    pub fn test_chunk_trait_impls() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();
        
        let chunk: Chunk = TryFrom::try_from(chunk_data.as_ref()).unwrap();
        
        let _chunk_string = format!("{}", chunk);
    }
}

