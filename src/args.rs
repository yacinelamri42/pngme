use std::{path::PathBuf, process};
use std::fs;
use clap::{Parser, Subcommand};
use crate::{chunk_type::ChunkType, chunk, png::Png};
use std::error::Error;

#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Args {
    #[command(subcommand)]
    command: Commands
}

#[derive(Subcommand, Debug)]
enum Commands {
    Encode {
        filepath: PathBuf,
        chunk_type: ChunkType,
        message: String,
        #[arg(short, long)]
        output_file: Option<PathBuf>
    },
    Decode {
        filepath: PathBuf,
        chunk_type: ChunkType,
    },
    Remove {
        filepath: PathBuf,
        chunk_type: ChunkType
    },
    Print {
        filepath: PathBuf
    }
}

impl Args {
    pub fn run(&self) {
        match &self.command {
            Commands::Encode { 
                filepath,
                chunk_type,
                message,
                output_file
            } => {
                let output: Vec<u8> = match Self::encode(filepath, chunk_type, message) {
                    Ok(output) => output,
                    Err(err) => {
                        eprintln!("Encoding Error: {err}");
                        process::exit(1);
                    }
                };
                match output_file {
                    Some(file) => {
                        fs::write(file, output).expect("Error in writing to file");
                    },
                    None => {
                        println!("{:#?}", output);
                    }
                }
            },
            Commands::Decode { 
                filepath,
                chunk_type 
            } => {
                let hidden_message = match Self::decode(filepath, chunk_type) {
                    Ok(message) => {
                        match message {
                            Some(m) => m,
                            None => {
                                eprintln!("No message hidden");
                                process::exit(1);
                            }
                        }
                    },
                    Err(err) => {
                        eprintln!("Decoding error: {err}");
                        process::exit(1);
                    }
                };
                println!("{hidden_message}");
            },
            Commands::Remove { 
                filepath,
                chunk_type
            } => {
                Self::remove(filepath, chunk_type).unwrap_or_else(|err| {
                    eprintln!("Remove Error: {err}");
                    process::exit(1);
                });
            },
            Commands::Print { 
                filepath
            } => {
                Self::print(filepath).unwrap_or_else(|err| {
                    eprintln!("Print Error: {err}");
                    process::exit(1);
                });
            }
        }
    }

    fn encode(filepath: &PathBuf, chunk_type: &ChunkType, message: &String) -> Result<Vec<u8>, Box<dyn Error>> {
        let new_chunk = chunk::Chunk::new(chunk_type.clone(), message.as_bytes().to_vec());
        let mut image = Png::try_from(&fs::read(filepath)?[..])?;
        image.append_chunk(new_chunk);
        Ok(image.as_bytes())
    }

    fn decode(filepath: &PathBuf, chunk_type: &ChunkType) -> Result<Option<String>, Box<dyn Error>> {
        let image = Png::try_from(&fs::read(filepath)?[..])?;
        let chunk = image.chunk_by_type(&chunk_type.to_string()[..]);
        match chunk {
            Some(c) => {
                Ok(Some(c.data_as_string()?))
            },
            None => {Ok(None)}
        }
    }

    fn remove(filepath: &PathBuf, chunk_type: &ChunkType) -> Result<(), Box<dyn Error>> {
        let mut image = Png::try_from(&fs::read(filepath.as_path())?[..])?;
        image.remove_chunk(&chunk_type.to_string()[..])?;
        fs::write(filepath, image.as_bytes())?;
        Ok(())
    }

    fn print(filepath: &PathBuf) -> Result<(), Box<dyn Error>> {
        let image = Png::try_from(&fs::read(filepath)?[..])?;
        println!("{:#?}", image.as_bytes());
        Ok(())
    }
}
